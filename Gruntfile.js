module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            min: {
                src: 'js/*.js',
                dest: 'dist/js/functions.min.js'
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'dist/css/style.css': ['stile.css']
                }
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: "./",
                    src: "image/*.{png,gif,jpg,jpeg}",
                    dest: "dist/"
                }]
            }
        },
        htmlrefs: {
            dist: {
                src: '**/*.html',
                dest: 'dist/'
            }
        },
        watch: {
            build: {
                files: ['index.html', 'css/*.css', 'js/*.js'],
                tasks: ['uglify', 'cssmin', 'imagemin', 'htmlrefs', 'browserSync']
            }
        },
        browserSync: {
    bsFiles: {
        src : '/index.html'
    },
    options: {
        server: {
            baseDir: "./"
        }
    }
}
    });
 
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-htmlrefs');
    grunt.loadNpmTasks('grunt-browser-sync');
 
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('build', ['uglify'], ['cssmin'], ['imagemin'], ['htmlrefs']);
};