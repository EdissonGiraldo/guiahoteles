module.exports = function (grunt) {
    var mozjpeg = require('imagemin-mozjpeg');
 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            min: {
                src: 'js/*.js',
                dest: 'dist/js/functions.min.js'
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'dist/css/style.css': ['css/main.css', 'css/img.css', 'css/text.css']
                }
            }
        },
        imagemin: {
            static: {
                options: {
                    optimizationLevel: 0,
                    svgoPlugins: [{
                        removeViewBox: false
                    }],
                    use: [mozjpeg()]
                },
                files: {
                    'dist/img/grunt-logo.png': 'img/grunt-logo.png', 
                    'dist/img/grunt-logo1.png': 'img/grunt-logo1.png'
                }
            }
        },
        htmlrefs: {
            dist: {
                src: 'index.html',
                dest: 'dist/index.html'
            }
        },
        watch: {
            build: {
                files: ['index.html', 'css/*.css', 'js/*.js'],
                tasks: ['uglify', 'cssmin', 'imagemin', 'htmlrefs']
            }
        }
    });
 
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-htmlrefs');
 
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('build', ['uglify'], ['cssmin'], ['imagemin'], ['htmlrefs']);
};


module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Aquí creamos las tareas
        sass: {
            dist: {
                options: {
                    sourcemap: 'none',
                },
                files: [{
                    expand: true,
                    cwd: 'sass',
                    src: ['**/*.scss'],
                    dest: 'css',
                    ext: '.css',
                }, ],
            },
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        "css/*./css",
                        "*.html",
                        "js/*.js"
                    ]
                }
                options: {
                    watchTask: true,
                    server: {
                        baseDir: "./"
                    }
                }
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: "./",
                    src: "image/*.{png,gif,jpg,jpeg}",
                    dest: "dist/"
                }]
            }
        },
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'css',
                    ext: '.min.css',
                }, ],
            },
        },
        watch: {
            css: {
                files: '**/*.scss',
                tasks: ['sass'],
            },
        }
    });

    // Cargamos los plugins de grunt
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-imagemin');

    // Registramos la tareas
    grunt.registerTask("css", ["sass"]);
    grunt.registerTask('default', ['browserSync', 'watch']);
    grunt.registerTask("img:compress", ["imagemin"]);
}